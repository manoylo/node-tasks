# Node.js test tasks

## Task1
Install: 
```
cd task1
npm i
```
Test:
```
npm test
```
Example #1:
``` 
echo Hello, world! | node filter.js
```
Result:
```
Hello, world!            | 48 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21 20 0d 0a
```
Example #2 - long text from file (caution: Windows only)
``` 
npm run file
```

## Task2
Install: 
```
cd task2
npm i
```
Test:
```
npm test
```
Example #1:
``` 
 curl -s --header "Content-Type: application/json" --request POST --data '{"x":1, "user":"xyz"}' http://localhost:3000/api/data
```
Result:
```
{"x":1,"user":"xyz","a":6}
```

Example #2:
``` 
 curl -s --header "Content-Type: application/json" --request POST --data '{"x":1, "command":"shutdown"}' http://localhost:3000/api/data
```
Result:
```
{"x":1,"command":"shutdown","a":2}
```

## Task3
Install: 
```
cd task3
npm i
```
Test:
```
npm test
```
Example #1:
``` 
 curl -s --header "Content-Type: application/json" --request GET http://localhost:3000/api/data
```

Example #2:
``` 
 curl -s --header "Content-Type: application/json" --request GET http://localhost:3000/api/data/5
```