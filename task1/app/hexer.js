/**
 * Hexer
 */
class Hexer {
    /**
     * @param string
     * @returns {string}
     */
    static toFormattedHex(string) {
        return Array.from(string).map(char => {
            return Buffer.from(char, 'utf8').toString('hex');
        }).join(' ');
    }
}

module.exports = Hexer;