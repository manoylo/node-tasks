const {Writable} = require('stream');
const {StringDecoder} = require('string_decoder');

const Hexer = require('../../app/hexer');


/**
 * HexTextStream
 */
class HexTextStream extends Writable {

    /**
     * @param {Terminal} terminal
     * @param options
     */
    constructor(terminal, options) {
        super(options);
        this._decoder = new StringDecoder(options && options.defaultEncoding);
        this.terminal = terminal;
    }

    /**
     * @param chunk
     * @param encoding
     * @param callback
     * @private
     */
    _write(chunk, encoding, callback) {
        if (encoding === 'buffer') {
            chunk = this._decoder.write(chunk);
        }
        chunk = Hexer.toFormattedHex(chunk);
        this.terminal.write(chunk);
        callback();
    }
}

module.exports = HexTextStream;