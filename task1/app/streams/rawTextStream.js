const {Transform} = require('stream');
const {StringDecoder} = require('string_decoder');


/**
 * RawTextStream
 */
class RawTextStream extends Transform {

    /**
     * @param {Terminal} terminal
     * @param options
     */
    constructor(terminal, options) {
        super(options);
        this._decoder = new StringDecoder('utf8');
        this.terminal = terminal;
    }

    /**
     * @param chunk
     * @param encoding
     * @param callback
     * @private
     */
    _transform(chunk, encoding, callback) {
        if (encoding === 'buffer') {
            chunk = this._decoder.write(chunk);
        }
        this.terminal.write(chunk);
        this.push(chunk);
        callback();
    }
}

module.exports = RawTextStream;