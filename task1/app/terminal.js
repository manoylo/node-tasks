/**
 * Terminal
 */
class Terminal {
    /**
     * @param side
     */
    constructor(side) {
        if (!['left', 'right'].includes(side)) {
            throw 'Invalid terminal side';
        }

        let columns = process.stdout.columns;

        this.line = 0;
        this.halfLength = Math.floor(columns / 2 - 2);
        this.side = side;
    }

    /**
     * @param content
     */
    write(content) {
        this._getLinesFromContent(content).forEach(line => {
            process.stdout.cursorTo(this._getX(), this.line++);
            this._writeLine(line);
        });
    }

    /**
     * @returns number
     * @private
     */
    _getX() {
        return this.side === 'left' ? 0 : this.halfLength;
    }

    /**
     * @param content
     * @returns Array
     * @private
     */
    _getLinesFromContent(content) {
        if (content.length > this.halfLength) {
            return content.match(new RegExp(`.{1,${this.halfLength}}`, 'g'));
        } else {
            return [content];
        }
    }

    /**
     * @param line
     * @private
     */
    _writeLine(line) {
        line = this.side === 'left' ? line : `| ${line} \n`;
        process.stdout.write(line);
    }
}

module.exports = Terminal;