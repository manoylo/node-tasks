const Terminal = require('./app/terminal');
const RawTextStream = require('./app/streams/rawTextStream');
const HexTextStream = require('./app/streams/hexTextStream');

let rawTerminal = new Terminal('left');
let rawStream = new RawTextStream(rawTerminal);

let hexTerminal = new Terminal('right');
let hexStream = new HexTextStream(hexTerminal);

process.stdin.setEncoding('utf8');
process.stdin.pipe(rawStream).pipe(hexStream);