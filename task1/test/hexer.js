const assert = require('assert');

let Hexer = require('../app/hexer');


describe('hexer', function() {
    it('should translate text into hex', function() {
        assert.equal(Hexer.toFormattedHex('Hello, world!\n'), '48 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21 0a');
    })

    it('should translate utf-8 text into hex', function() {
        assert.equal(Hexer.toFormattedHex('𝟙𝟚𝟛\n'), 'f09d9f99 f09d9f9a f09d9f9b 0a');
    })
});