class Controller {
    static serveData(req, res, next) {
        req.body.a = Math.round(Math.random() * 5 + 1);
        res.send(req.body);
        next();
    }

    static processCommand(req, res, next) {
        if (req.body.command === 'shutdown') {
            process.exit(2);
        }
    }
}

module.exports = Controller;