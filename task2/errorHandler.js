module.exports = (error, req, res, next) => {
    console.error(error);
    res.status(400).send({
        error: 'Invalid input'
    });
};