const express = require('express');
const bodyParser = require('body-parser');

const errorHandler = require('./errorHandler');
const Controller = require('./controller');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(errorHandler);

app.post('/api/data', [
    Controller.serveData,
    Controller.processCommand
]);

app.listen(port, () => console.log(`Example app listening on port ${port}`));

module.exports = app;