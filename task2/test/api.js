let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('/api/data', () => {
    describe('POST /api/data', () => {
        it('it should add "a" key', (done) => {
            let sample = {
                x: 5,
                y: 10
            };
            chai.request(server)
                .post('/api/data')
                .send(sample)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.include(sample);
                    res.body.should.have.property('a');
                    res.body.a.should.be.at.least(1).and.at.most(6);
                    done();
                });
        });
    });
});
