const request = require('request');

const DATA_SOURCE_URL = 'https://jsonplaceholder.typicode.com/todos/';


class Controller {
    static serveData(req, res, next) {
        request({
            url: DATA_SOURCE_URL + `/${req.params.id || ''}`
        }).on('error', error => {
            next(error);
        }).pipe(res);
    }
}

module.exports = Controller;