const express = require('express');

const errorHandler = require('./errorHandler');
const Controller = require('./controller');

const app = express();
const port = 3000;

app.use(errorHandler);

app.get('/api/data/:id?', [
    Controller.serveData
]);

app.listen(port, () => console.log(`Example app listening on port ${port}`));

module.exports = app;