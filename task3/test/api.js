let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('/api/data', () => {
    describe('GET /api/data', () => {
        it('it should return list of todos', (done) => {
            chai.request(server)
                .get('/api/data')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.should.have.length.above(1);
                    done();
                });
        });
    });

    describe('GET /api/data/:id', () => {
        it('it should return single todo by id', (done) => {
            chai.request(server)
                .get('/api/data/5')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
});
